﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MvpSample.UI
{
    interface IInterestUI
    {
        string StartCapital { get; set; }
        string YearInterest { get; set; }
        string MonthCount { get; set; }
        string ResultCapital { get; set; }

        event CalculationHandler CalculationStated;
    }

    public delegate void CalculationHandler();
    public class InterestUI
    {
        public string StartCapital { get; set; }
        public string YearInterest { get; set; }
        public string MonthCount { get; set; }
        public string ResultCapital { get; set; }

        public event CalculationHandler CalculationStated;
    }
}
